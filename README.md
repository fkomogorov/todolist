# TodoList

Basic ToDo List made with HTML+CSS, JS and jQuery. You can add items, scratch them off or entirely delete them.